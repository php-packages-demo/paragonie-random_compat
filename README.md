# paragonie/random_compat

PHP 5.x-7 polyfill for random_bytes() and random_int() from PHP 7. https://packagist.org/packages/paragonie/random_compat

[![PHPPackages Rank](http://phppackages.org/p/paragonie/random_compat/badge/rank.svg)](http://phppackages.org/p/paragonie/random_compat)
[![PHPPackages Referenced By](http://phppackages.org/p/paragonie/random_compat/badge/referenced-by.svg)](http://phppackages.org/p/paragonie/random_compat)